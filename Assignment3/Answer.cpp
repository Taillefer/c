/*************************************************************************************************
* FileName: Answer.cpp
* Version:  V1.0
* Author:   Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 C++ Programming
* Lab Section: 012
* Assignment Number: 3
* Assignment Name: Polymorphism
* Due Date: April 13,2015
* Submission Date: April 10,2015
* Professor's Name: Andrew Tyler
* Purpose: Answer.cpp to create a base class Answer. Answer's cannot be directly created but can be derived from
*************************************************************************************************/
#include "Answer.h"

/*************************************************************************************************
* Function Name: Answer()
* Purpose: Overloaded constructor, to create Answer object
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Answer::Answer(bool b_correct)
{
	this->bCorrect = b_correct;
}

/*************************************************************************************************
* Function Name: Answer()
* Purpose: Copy consturctor to copy answer objects
* Function in: Answer&
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Answer::Answer(Answer& answer)
{
	this->bCorrect = answer.bCorrect;
}

/*************************************************************************************************
* Function Name: operator=
* Purpose: Overloaded assignment operator, to set the referenced answer to current answer
* Function in: Answer&
* Function out: Answer&
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Answer& Answer::operator=(Answer& answer)
{
	this->bCorrect = answer.bCorrect;
	return *this; //Return copy of the object
}

/*************************************************************************************************
* Function Name: ~Answer()
* Purpose: Destructor for Answer objects
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Answer::~Answer() {
}