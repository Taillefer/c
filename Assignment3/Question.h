struct Question
{
	static const unsigned int MAX_ANSWERS = 3;
	Question();
	Question(char*, unsigned int, Answer**);
	Question(Question&);
	~Question();
	Question& operator=(Question&);
	bool operator==(Question&);
	friend ostream& operator<<(ostream&, Question&);
private:
	char* qText;
	unsigned int mark;
	Answer** answers;
};