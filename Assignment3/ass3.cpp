#include "Answer.h"
#include "FactualAnswer.h"
#include "NumericalAnswer.h"
#include "Question.h"
#include "Exam.h"
int main(void)
{
	char i_response;
	unsigned int index;
	bool b_running = true;
	Exam e;

	while (b_running)
	{
		cout << "\nPlease enter your choice\n";
		cout << "1. Add a New Question\n";
		cout << "2. Delete a Question\n";
		cout << "3. Print a question\n";
		cout << "4. Print all the Questions\n";
		cout << "5. Quit\n";
		cin >> i_response;

		switch (i_response)
		{
		case '1':
			if (!e.AddNewQuestion())
				return 1;
			break;
		case '2':
			if (!e.DeleteQuestion())
				return 1;
			break;
		case '3':
			cout << "please enter the question number to print: ";
			cin >> index;
			cout << e[index];
			break;
		case '4':
			cout << e;
			break;
		case '5':
			b_running = false;
			break;
		}
	}
	return 0;
}