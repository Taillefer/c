class Exam
{
	static const unsigned int MAX_QUESTIONS = 4;
	char* title;
	Question questions[MAX_QUESTIONS];
public:
	Exam();
	~Exam();
	bool AddNewQuestion();
	bool DeleteQuestion();
	Question operator[](unsigned int);
	friend ostream& operator<<(ostream&, Exam&);
};