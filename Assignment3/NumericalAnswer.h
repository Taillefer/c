class NumericalAnswer :public Answer
{
	double fScore;
public:
	NumericalAnswer() :Answer(), fScore(0){}
	NumericalAnswer(double, bool);
	NumericalAnswer(NumericalAnswer&);
	~NumericalAnswer();
	friend ostream& operator<<(ostream&, NumericalAnswer&);
	void Report(){ cout << *this; }
};