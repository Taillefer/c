/*************************************************************************************************
* FileName: FactualAnswer.cpp
* Version:  V1.0
* Author:   Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 C++ Programming
* Lab Section: 012
* Assignment Number: 3
* Assignment Name: Polymorphism
* Due Date: April 13, 2015
* Submission Date: April 10,2015
* Professor's Name: Andrew Tyler
* Purpose: FactualAnswer, derived class from Answer.Includes default constructor, Overloaded constructor, Copy constructor, Destructor, operator<<
*************************************************************************************************/
#include "Answer.h"
#include "FactualAnswer.h"
#include <iomanip>

/*************************************************************************************************
* Function Name: FactualAnswer()
* Purpose: Default constructor to create a FactualAnswer
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
FactualAnswer::FactualAnswer() {
	this->aText = 0; //Set text to 0
}

/*************************************************************************************************
* Function Name: FactualAnswer()
* Purpose: Overloaded constructor to create a FactualAnswer
* Function in: Char *, bool
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
FactualAnswer::FactualAnswer(char * text, bool correct) {
	this->aText = new char[strlen(text) + 1]; //Allocate space for size of string + 1
	strcpy(this->aText, text); //Strcpy to empty memory location 
	this->bCorrect = correct; //Set bCorrect
}

/*************************************************************************************************
* Function Name: FactualAnswer()
* Purpose: Copy constructor to copy a FactualAnswer
* Function in: FactualAnswer&
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
FactualAnswer::FactualAnswer(FactualAnswer& factualAnswer) :Answer(factualAnswer.bCorrect) {
	this->aText = new char[strlen(factualAnswer.aText) + 1]; //Allocate space for size of string 
	strcpy(this->aText, factualAnswer.aText);  //Strcpy to empty memory location
}

/*************************************************************************************************
* Function Name: ~FactualAnswer()
* Purpose: Destructor for Factual Answer
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
FactualAnswer::~FactualAnswer() {
	if (this->aText != 0) { //If the text exists delete it
		delete aText;
	}
}

/*************************************************************************************************
* Function Name: operator<<
* Purpose: Overloaded output operator to print Factual Answer
* Function in: ostream&, FactualAnswer&
* Function out: ostream&
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
ostream& operator<<(ostream& ostream, FactualAnswer& factualAnswer) {
	if (factualAnswer.aText != 0) { //If there is a text, there's a Factual Answer to print
		char *string = factualAnswer.bCorrect ? "Correct" : "False"; //Ternary if, deceide whether to print true or false
		cout << "Text = " << setw(42) << left << factualAnswer.aText << string << "\n";
	}
	return ostream;
}