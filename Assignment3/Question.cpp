/*************************************************************************************************
* FileName: Question.cpp
* Version:  V1.0
* Author:   Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 C++ Programming
* Lab Section: 012
* Assignment Number: 3
* Assignment Name: Polymorphism
* Due Date: April 13, 2015
* Submission Date: April 10, 2015
* Professor's Name: Andrew Tyler
* Purpose: Create a Question for the exam. Includes default constructor, Overloaded constructor, Copy constructor, Destructor, overloaded operator<<, and overloaded == operator
*************************************************************************************************/
#include "Answer.h"
#include "FactualAnswer.h"
#include "NumericalAnswer.h"
#include "Question.h"
#include <string.h>
#include <ostream>

/*************************************************************************************************
* Function Name: ~Question()
* Purpose: Destructor for question objects
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Question::~Question(){
	delete qText; //Delete memory allocated for qText
	qText = 0; //Null the pointer
	for (int i = 0; i<MAX_ANSWERS; i++) { //Loop through the Answers ofr the question
		if (this->answers) { //If the pointer to answers exists
			if (this->answers[i]) { //(f the index in pointer to answers exists
				delete this->answers[i]; //Delete the answer at the index i
				this->answers[i] = 0; //Null the pointer
			}
			else { //Else there was no answer at index
				break;
			}
		}
		else { //There was no answer pointers
			break;
		}
	}
	delete this->answers; //Delete answer pointers
	this->answers = 0; //Null the pointer
}

/*************************************************************************************************
* Function Name: Question()
* Purpose: Default constructor for Question object
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Question::Question() {
	qText = 0; // Allocate the question pointer to null
	answers = NULL; //Allocate the answers pointer to pointer to null
}

/*************************************************************************************************
* Function Name: Overloaded constructor to create question object
* Purpose: Overloaded constructor to create a NumericalAnswer
* Function in: char*,unsigned int, Answer**
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Question::Question(char* text, unsigned int mark, Answer** answer)
{
	if (text == 0) { //If the text is null, set the question text to null
		qText = text;
	}
	else {
		this->qText = new char[strlen(text) + 1]; //Allocate space for the text
		strcpy(this->qText, text);
	}
	this->mark = mark;
	if (this->mark != -1) //Check to see if the question is a valid question
	{
		this->answers = answer;
	}
	else { //Else set the answer pointer to null 
		this->answers = 0;
	}
}

/*************************************************************************************************
* Function Name: Question()
* Purpose: Copy constructor to copy a question object
* Function in: Question&
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Question::Question(Question& question)
{
	if (question.qText == 0) { //If the question text is null, set the question text to null
		qText = 0;
	}
	else { //Create memory for the text
		this->qText = new char[strlen(question.qText) + 1]; //Allocate space for the text
		strcpy(this->qText, question.qText);
	}
	this->mark = question.mark;
	if (!question.answers) { //If the question doesnt have a valid Answer**
		this->answers = 0; //Set the answer to 0 and return the Question
		return;
	}
	this->answers = new Answer*[MAX_ANSWERS]; //Allocate memory for the answers
	for (int i = 0; i<MAX_ANSWERS; i++) { //Loop through upto max Answers
		this->answers[i] = NULL; //Null the current index
		if (dynamic_cast<NumericalAnswer*>(question.answers[i])) { //Check to see if the question's answer at index i is a NumericakAnswer
			NumericalAnswer* answer = dynamic_cast<NumericalAnswer*>(question.answers[i]); //Create pointer to a new NumericalAnswer
			this->answers[i] = new NumericalAnswer(*answer); //Set the Answer to the NumericalAnswer with NumericalAnswer copy constructor
		}
		else if (dynamic_cast<FactualAnswer*>(question.answers[i])) { //Check to see if the question's answer at index i is a FactualAnswer
			FactualAnswer* answer = dynamic_cast<FactualAnswer*>(question.answers[i]); //Create pointer to a new FactualAnswer
			this->answers[i] = new FactualAnswer(*answer); //Set the answer to the FactualAnswer with FactualAnswer copy constructor
		}
		else { //Else there is no question at the index, break loop
			break;
		}
	}
}

/*************************************************************************************************
* Function Name: operator=
* Purpose: Operator= to set a Question to the referenced question parameter
* Function in: Question&
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Question& Question::operator=(Question& question) {
	int i = 0;
	if (question.qText == 0) { //If the text is 0, set the question text to 0
		qText = 0;
	}
	else { //Else allocate memory for the question text
		this->qText = new char[strlen(question.qText) + 1];
		strcpy(this->qText, question.qText);
	}
	this->mark = question.mark;
	if (!question.answers) { //If the pointer to answers* doesn't exist
		this->answers = 0; //Null the pointer
		return *this; //Return the Question
	}
	this->answers = new Answer*[MAX_ANSWERS]; //Allocate memory for the answers
	for (int i = 0; i<MAX_ANSWERS; i++) { //Loop through the answers
		this->answers[i] = NULL; //Null the answer at index i
		if (question.answers) { //If the question answer exists
			if (dynamic_cast<NumericalAnswer*>(question.answers[i])) { //Check to see if the answer at index i is a NumericalAnswer
				NumericalAnswer* answer = dynamic_cast<NumericalAnswer*>(question.answers[i]); //Create pointer to a new NumericalAnswer
				this->answers[i] = new NumericalAnswer(*answer); //Set the answer to the NumericalAnswer with NumericalAnswer copy constructor
			}
			else if (dynamic_cast<FactualAnswer*>(question.answers[i])) { //Check to see if the answer at index i is a FactualAnswer
				FactualAnswer* answer = dynamic_cast<FactualAnswer*>(question.answers[i]); //Create pointer to a new FactualAnswer
				this->answers[i] = new FactualAnswer(*answer); //Set the answer to the FactualAnswer with FactualANswer copy constructor
			}
			else { //Else question answer** doesn't exist
				break;
			}
		}
	}
	return *this;
}

/*************************************************************************************************
* Function Name: operator<<
* Purpose: Overloaded << operator to output question objects
* Function in: ostream&, Question&
* Function out: ostream&
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
ostream& operator<<(ostream& RO, Question& RQ) {
	int i = 0;
	if (RQ.mark != -1) { //If the question mark isn't -1 (Invalid question)
		if (RQ.qText) { //If the question text is valid
			RO << RQ.qText << "\n"; 
			for (i = 0; i<RQ.MAX_ANSWERS; i++) { //Loop through question 
				if (dynamic_cast<FactualAnswer*>(RQ.answers[i]) || dynamic_cast<NumericalAnswer*>(RQ.answers[i])) { //If the question answer is either Numerical or Factual
					cout << "Ans # " << i + 1 << ":  ";
					RQ.answers[i]->Report(); //Polymorphism to print the Answer
				}
				else { //Answer was invalid
					break;
				}
			}
		}
		else { //There was no question at that index
			RO << "Invalid question index, no question exists";
		}
	}
	else { //Else There is no Question available
		RO << "Invalid question index, no question exists";
	}
	RO << "\n\n";
	return RO;
}

/*************************************************************************************************
* Function Name: operator==
* Purpose: Overloaded operator ==, to check and see if the question's text is 0
* Function in: bool 
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
bool Question::operator==(Question& question) {
	if (qText == 0) { //if question text is null, return true
		return true;
	}
	return false; //Else return false
}

