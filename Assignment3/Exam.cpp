/*************************************************************************************************
* FileName: Exam.cpp
* Version:  V1.0
* Author:   Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 C++ Programming
* Lab Section: 012
* Assignment Number: 3
* Assignment Name: Polymorphism
* Due Date: April 13, 2015
* Submission Date: April 10, 2015
* Professor's Name: Andrew Tyler
* Purpose: Exam.cpp for the Exam class, has the default constructor, destructor, overloaded constructor, copy constructor, and the following function, AddNewQuestion, DeleteQuestion,
*			And the overloaded operator's [] and <<
*************************************************************************************************/
#include "Answer.h"
#include "FactualAnswer.h"
#include "NumericalAnswer.h"
#include "Question.h"
#include "Exam.h"
/*************************************************************************************************
* Function Name: Exam()
* Purpose: Default constructor to create an Exam object 
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Exam::Exam() {
	char tempTitle[255]; //Allocate max char size as a buffer 
	cout << "What is the title of the exam?\n";
	cin.getline(tempTitle, 100); //Get the user input upto a max of 100 chars
	title = new char[strlen(tempTitle) + 1];
	strcpy(title, tempTitle);
	fflush(stdin);
}

/*************************************************************************************************
* Function Name: ~Exam()
* Purpose: Destructor for Exam object
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Exam::~Exam() {
	Question q;
	delete[]title; //Delete the exam title
	for (int i = 0; i<MAX_QUESTIONS; i++) {
		if ((questions[i] == q) == false) {
			questions[i].~Question();
		}
		else {
			break;
		}
	}
	_CrtDumpMemoryLeaks();
}

/*************************************************************************************************
* Function Name: AddNewQuestion()
* Purpose: Add a new question to the exam
* Function in: None
* Function out: bool (true/false)
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
bool Exam::AddNewQuestion() {
	int i, j, numAnswers = 0, correct; //Variables for counters
	int qMark = 0; //Questions mark
	double fscore = 0; //NumericalAnswer's score
	Question question; //Pointer to Question
	Answer** localAnswers = new Answer*[questions->MAX_ANSWERS]; //Local ** to Answer's
	for (int i = 0; i<questions->MAX_ANSWERS; i++) { //Allocate each pointer to  0
		localAnswers[i] = NULL;
	}
	if (MAX_QUESTIONS <= 0) { // If the MAX_QUESTIONS is invalid
		cout << "Error: MAX_QUESTIONS IS : " << MAX_QUESTIONS << ".\n";
		return true;
	}
	for (i = 0; i < MAX_QUESTIONS; i++)  //Find the next question that can be used
	{
		if ((questions[i] == question) == true) //If the question is 0, we found the next question to use
		{
			break;
		}
		else if (i == MAX_QUESTIONS - 1) { //We have reached MAX_QUESTIONS we cannot add anymore
			delete localAnswers; //Deallocate memory for localAnswers
			printf("Reached Max Questions");
			return true;
		}
	}
	fflush(stdin);
	cout << "Please enter the new question text: ";
	char tempQText[255]; //Temp buffer for the question text
	cin.getline(tempQText, 255); //Get user input
	cout << "\nPlease enter the new question mark: ";
	cin >> qMark; //Take user input for question's mark
	if (question.MAX_ANSWERS <= 0) {
		cout << "Error: MAX_ANSWERS is : " << question.MAX_ANSWERS << ".\n";
		return true;
	}

	cout << "\nPlease enter the number of answers, up to a max of : " << question.MAX_ANSWERS << "\n";
	cin >> numAnswers; //Take user input for number of answers for the question
	while (numAnswers <= 0 || numAnswers > question.MAX_ANSWERS) //Check if the user's input is valid
	{
		cout << "Error: please enter the number of answers, up to a max of " << question.MAX_ANSWERS << ": ";
		cin >> numAnswers;
	}

	for (j = 0; j<numAnswers; j++) //Iterate through for the number of answer's they wish to enter
	{
		char choice;
		char tempAText[255]; //Temp location for Answer text
		cout << "Is the next answer Factual (F) or Numerical (N):?\n";
		cin >> choice;
		if (choice != 'N' && choice != 'F') { //If the user's choice isn't F or N, reask the user
			while (choice != 'N' && choice != 'F') { //Continue to loop until valid answer
				cout << "Error: Please enter a valid answer, Numerical (N) or Factual(F)?: ";
				cin >> choice;
			}
		}
		if (choice == 'F') { //If user wants a factual answer
			cout << "Please enter the next Factual answer " << j + 1 << " \n";
			fflush(stdin);
			cin.getline(tempAText, 255); //Get Factual text
			fflush(stdin);
			cout << "Is it correct? (0 = no, 1= yes)?: ";
			cin >> correct; //Is is Answer correct ?
			while (correct != 1 && correct != 0) //Check to see if user's input is valid
			{
				cout << "Error: Please enter a valid correct answer (0 = no, 1 =yes)?: ";
				cin >> correct;
			}
			Answer* answers = new FactualAnswer(tempAText, correct); //Create a new pointer to a Factual Answer using overloaded Factual constructor
			localAnswers[j] = answers; //Point the index to the newly created FactualAnswer
		}
		else if (choice == 'N') { //If the user wants a numerical Answer
			cout << "Please enter the next Numerical answer" << j + 1 << " \n";
			cin >> fscore; //Get answer double value
			fflush(stdin);
			cout << "Is it correct? (0 = no, 1 =yes)?: ";
			cin >> correct; //Get if the answer is correct
			while (correct != 1 && correct != 0) //Check to see if user's input is valid
			{
				cout << "Error: Please enter a valid correct answer (0 = no, 1 =yes)?: ";
				cin >> correct;
			}
			Answer *answers = new NumericalAnswer(fscore, correct); //Create a new pointer to a Numerical Answer using overloaded Numerical constructor
			localAnswers[j] = answers; //Point the index to the newly created NumericalAnswer
		}
	}
	question = Question(tempQText, qMark, localAnswers); //Create new question with the given attributes
	questions[i] = question; //Allocate the index to the Created Question
	return true;
}


/*************************************************************************************************
* Function Name: DeleteQuestion()
* Purpose: Delete a question in the exam object
* Function in: None
* Function out: bool (true/false)
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
bool Exam::DeleteQuestion() {
	int qToDelete = 0;
	int i = 0;
	Question q = Question(0, -1, NULL); //Create an invalid empty Question
	cout << "Please enter the number of question to delete: (Upto " << MAX_QUESTIONS << ")"; 
	cin >> qToDelete; //Get which question they wish to delete
	while (qToDelete <= 0 || qToDelete > MAX_QUESTIONS) //If the question they want to delete is within the range
	{
		fflush(stdin);
		cout << "Error: Please enter the number of question to delete: (Upto " << MAX_QUESTIONS << ")";
		cin >> qToDelete;
	}
	fflush(stdin);
	if ((questions[qToDelete - 1]) == q){ //If the Question pointer is 0, the user tried to delete an empty question
		cout << "Empty Question";
	}
	else { // We can delete the question 
		while ((questions[i + (qToDelete - 1)] == q) == false) {  //While the next question is still a valid question 
			if (qToDelete == MAX_QUESTIONS) { //If the user wishes to delete the max Question
				questions[qToDelete - 1].~Question(); //Deallocate memory for the question
				break;
			}
			if ((i + (qToDelete - 1)) == MAX_QUESTIONS - 1) { //If the uer wishes to delete the 2nd last question
				questions[i + (qToDelete - 1)].~Question();
				break;
			}
			if ((questions[i + (qToDelete)] == q) == true) { //If the next question is not a valid question
				questions[i + (qToDelete - 1)].~Question();
			}
			else { //Else we wish to delete the question and move the next question to the current spot
				questions[i + (qToDelete - 1)].~Question();
				questions[i + (qToDelete - 1)] = questions[i + (qToDelete)];
			}
			i++;
		}
		cout << "Deleting question " << qToDelete << "\n";
	}
	return true;
}

/*************************************************************************************************
* Function Name: operator[]
* Purpose: Overloaded []  operator to return a single question from the exam
* Function in: unsigned int index
* Function out: Question
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
Question Exam::operator[](unsigned int index)
{
	Question question;
	if ((this->questions[index - 1] == question) == false && index > 0 && index <= MAX_QUESTIONS) //If the current question exists and is within index range
	{
		question = Question(this->questions[index - 1]); //Copy the required question 
		return question;
	}
	question = Question(0, -1, NULL); //Else we have a invalid question, create question with invalid information
	return question;
}

/*************************************************************************************************
* Function Name: operator<<
* Purpose: Overloaded output operator for the 
* Function in: ostream&, Exam&
* Function out: ostream&
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
ostream& operator<<(ostream& ostream, Exam& exam)
{
	ostream << "Title " << exam.title << "\n";
	for (int i = 0; i<exam.MAX_QUESTIONS; i++) //Iterate through the questions
	{
		Question q;
		if ((exam.questions[i] == q) == false)  //If the question exists we should print it
		{
			ostream << "Question " << i + 1 << ": \n";
			Question question = Question(exam.questions[i]); //Question copy constructor to create new Question
			ostream << question; //Call overloaded output operator for question

		}
		else //Else we have no more question's to print
		{
			if (i == 0) { //If we are on the first iteration and find no Questions, there are none.
				cout << "No Questions" << "\n";
			}
			break;
		}
	}
	return ostream;
}