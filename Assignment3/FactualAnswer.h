class FactualAnswer :public Answer
{
	char* aText;
public:
	FactualAnswer();
	FactualAnswer(char*, bool);
	FactualAnswer(FactualAnswer&);
	~FactualAnswer();
	friend ostream& operator<<(ostream&, FactualAnswer&);
	void Report(){ cout << *this; };
};