/*************************************************************************************************
* FileName: NumericalAnswer.cpp
* Version:  V1.0
* Author:   Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 C++ Programming
* Lab Section: 012
* Assignment Number: 3
* Assignment Name: Polymorphism
* Due Date: April 13, 2015
* Submission Date: April 10,2015
* Professor's Name: Andrew Tyler
* Purpose: NumericalAnswer, derived class from Answer.Includes default constructor, Overloaded constructor, Copy constructor, Destructor, operator<<
*************************************************************************************************/

#include "Answer.h"
#include "NumericalAnswer.h"
#include <iomanip>

/*************************************************************************************************
* Function Name: NumericalAnswer()
* Purpose: Overloaded constructor to create a NumericalAnswer
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
NumericalAnswer::NumericalAnswer(double fscore, bool bCorrect) :Answer(bCorrect) {
	this->fScore = fscore;
}

/*************************************************************************************************
* Function Name: FactualAnswer()
* Purpose: Copy constructor to copy the NumericalAnswer
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
NumericalAnswer::NumericalAnswer(NumericalAnswer& numericalAnswer) : Answer(numericalAnswer.bCorrect) {
	this->fScore = numericalAnswer.fScore;
}

/*************************************************************************************************
* Function Name: operator<<
* Purpose: Overloaded output operator to print a NumericalANswer
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
ostream& operator<<(ostream& ostream, NumericalAnswer& answer) {
	char *string = answer.bCorrect ? "Correct" : "False"; //Deceide whether to print true or false
	cout << "Number = " << setw(40) << left << answer.fScore << string << "\n";
	return ostream;
}


/*************************************************************************************************
* Function Name: ~NumericalAnswer()
* Purpose: Destructor for NumericalAnswer
* Function in: None
* Function out: None
* Version: V1.0
* Author: Brodie Taillefer
* ************************************************************************************************/
NumericalAnswer::~NumericalAnswer() {

}