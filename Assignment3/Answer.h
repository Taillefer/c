#include <iostream>
using namespace std;
class Answer
{
protected:
	bool bCorrect;
public:
	Answer() :bCorrect(false){}
	Answer(bool);
	Answer(Answer&);
	virtual ~Answer();
	Answer& operator=(Answer&);
	virtual void Report() = 0;
};