#include <iostream>
using namespace std;
class Answer
{	
	char* aText;
	bool b_correct;
public:	
	Answer():aText(0){;}
	Answer(char*,bool);
	Answer(Answer&);
	~Answer();
	Answer& operator=(Answer&);
	friend ostream& operator<<(ostream&,Answer&);
};