struct Question
{
	static const unsigned int MAX_ANSWERS = 5;
	Question():qText(0){;}
	Question(char*,unsigned int,Answer*);
	Question(Question&);
	~Question();
	friend ostream& operator<<(ostream&,Question&);
private:
	char* qText;
	unsigned int mark;
	Answer answers[MAX_ANSWERS];
};