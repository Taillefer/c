/********************************************************************************************************
* FileName: ass2.cpp
* Version: 1.0
* Author: Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 - C++
* Lab section: 012
* Assignment Number: 1
* Assignment Name: Overloaded Operators
* Due Date: March 16, 2015
* Submission Date: March 16, 2015
* Professor's Name: Andrew Tyler
* Purpose: Main for assignment2, allow's the user the choose from the menu, calling the desired function
********************************************************************************************************/
#include "Answer.h"
#include "Question.h"
#include "Exam.h"
#include <iostream>
using namespace std;

/********************************************************************************************************
* Function Name: main
* Purpose: main for assignment2
* Function in parameters: void
* Function out parameters: int
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
int main(void)
{
	char i_response;
	unsigned int index;
	bool b_running = true;
	Exam e;

	while(b_running) 
	{
		cout<<"\nPlease enter your choice\n";
		cout<<"1. Add a new Question\n";
		cout<<"2. Delete a Question\n";
		cout<<"3. Print a Question\n";
		cout<<"4. Print all the Quesitons\n";
		cout<<"5. Quit\n";
		cin>>i_response;

		switch(i_response)
		{
		case '1':
			if(!e.AddNewQuestion())
				return 1;
			break;
		case '2':
			if(!e.DeleteQuestion())
				return 1;
			break;
		case '3':
			cout<<"Please enter the question number to print: ";
			cin>>index;
			cout<<e[index];
			break;
		case '4':
			cout<<e;
			break;
		case '5':
			b_running = false;
			break;
		}
	}
	return 0;
}


