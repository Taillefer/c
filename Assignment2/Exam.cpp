/********************************************************************************************************
* FileName: Exam.cpp
* Version: 1.0
* Author: Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 - C++
* Lab section: 012
* Assignment Number: 2
* Assignment Name: Overloaded Operators
* Due Date: March 15, 2015
* Submission Date: March 15, 2015
* Professor's Name: Andrew Tyler
* Purpose: Exam.cpp, Includes the Constructor's to create Exam's, deconstructor to release memory, functions to
*		   add a new question, delete a question, print all questions, and the overloaded operator [] and <<
********************************************************************************************************/

#include "Answer.h"
#include "Question.h"
#include "Exam.h"
#include <iostream>
#include <string.h>
#include <crtdbg.h>
using namespace std;

/********************************************************************************************************
* Function Name: Exam
* Purpose: Default Constructor for Exam class, creates Exam with user input
* Function in parameters: None
* Function out parameters:None
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
Exam::Exam() {
	char tempTitle[255]; //Allocate max char size as a buffer 
	cout<<"What is the title of the exam?\n";
	cin.getline(tempTitle,100); //Get the user input upto a max of 100 chars
	title = new char[strlen(tempTitle)+1];
	strcpy(title,tempTitle);
	fflush(stdin);
	for(int i=0;i<MAX_QUESTIONS;i++) //Intialize all the questions with 0
	{
		questions[i] = 0;
	}
}

/********************************************************************************************************
* Function Name: ~Exam
* Purpose: Deconstructor for the Exam,  deletes the dynamic memory for the Question's and exam title 
* Function in parameters: None
* Function out parameters:None
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
Exam::~Exam() {
	int i=0;
	for(i=0;i<this->MAX_QUESTIONS;i++) { 
		if(this->questions[i]) {//While the question is valid and exists
			delete questions[i]; //Delete the pointer to question at index i
		}
		else {
			break;
		}
	}
	delete[] title; //Delete the exam title
	_CrtDumpMemoryLeaks();
}

/********************************************************************************************************
* Function Name: AddNewQuestion
 Purpose:	Add a new question to the Exam and it's Answers. Asks user input for Question text, and marks. Checking 
*		   to see if the what the user enter's is within valid range.
* Function in parameters: None
* Function out parameters: bool
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
bool Exam::AddNewQuestion() {
	int i,j,numAnswers = 0,correct; //
	int qMark = 0; //Questions mark
	Question *question; //Pointer to Question
	Answer answer[question->MAX_ANSWERS]; //Array of answers 
	if(MAX_QUESTIONS <= 0) { // If the MAX_QUESTIONS is invalid
		cout<<"Error: MAX_QUESTIONS IS : "<<MAX_QUESTIONS<<".\n";
		return true;
	}
	for(i = 0;i < MAX_QUESTIONS; i++)  //Find the next question that can be used
	{
		if(!questions[i]) //If the question is 0, we found the next question to use
		{
			break;
		}
		else if(i == MAX_QUESTIONS-1) { //We have reached MAX_QUESTIONS we cannot add anymore
			printf("Reached Max Questions");
			return true;
		}
	}
	fflush(stdin);
	cout<<"Please enter the new question text: ";
	char tempQText[255]; //Temp buffer for the question text
	cin.getline(tempQText,255); //Get user input
	cout<<"\nPlease enter the new question mark: ";
	cin>>qMark; //Take user input for question's mark
	if(question->MAX_ANSWERS <=0) {
		cout<<"Error: MAX_ANSWERS is : "<<question->MAX_ANSWERS<<".\n";
		return true;
	}
	cout<<"\nPlease enter the number of answers, up to a max of : "<<question->MAX_ANSWERS<<"\n";
	cin>>numAnswers; //Take user input for number of answers for the question
	while(numAnswers <=0 || numAnswers > question->MAX_ANSWERS) //Check if the user's input is valid
	{
		cout<<"Error: please enter the number of answers, up to a max of "<<question->MAX_ANSWERS<<": ";
		cin>>numAnswers;
	}
	for(j=0;j<numAnswers;j++) //Iterate through for the number of answer's they wish to enter
	{
		char tempAText[255]; //Temp location for Answer text
		cout<<"Please enter answer "<<j+1<<" \n";
		fflush(stdin);
		cin.getline(tempAText,255); 
		cout<<"Is it correct? (0 = no, 1= yes)?: ";
		cin>>correct; 
		while(correct != 1 && correct != 0) //Check to see if user's input is valid
		{
			cout<<"Error: Please enter a valid correct answer (0 = no, 1 =yes)?: ";
			cin>>correct;
		}
		Answer answers(tempAText,correct); //Create new Answer with the given parameters
		answer[j] = answers; //Set the index at j to the answer
	}
	question = new Question(tempQText,qMark,answer); //Create new question with the given attributes
	questions[i] = question; //Allocate the array of pointers to Question objects at index i to our question.
	return true;
}

/********************************************************************************************************
* Function Name: DeleteQuestion
* Purpose: Used to delete a question in the Exam object. Once the quesiton is deleted, the array of pointers
*		   to questions is packed to leave no empty spots.
* Function in parameters: None
* Function out parameters: bool
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
bool Exam::DeleteQuestion()
{
	int qToDelete = 0;
	int i = 0;
	cout << "Please enter the number of question to delete: (Upto " << MAX_QUESTIONS << ")";
	cin >> qToDelete;
	while (qToDelete <= 0 || qToDelete > MAX_QUESTIONS) //If the question they want to delete is within the range
	{
		fflush(stdin);
		cout << "Error: Please enter the number of question to delete: (Upto " << MAX_QUESTIONS << ")";
		cin >> qToDelete;
	}
	fflush(stdin);
	if (!questions[qToDelete - 1]) { //If the Question pointer is 0, the user tried to delete an empty question
		cout << "Empty Question";
	}
	else {
		//We need to pack the array of pointers to questions until it is fully packed, advance each index after the deletion foward and null the old location of the one that was moved
		while (questions[i + (qToDelete -1)]) // While the current value we are looking at is not NULL
		{
			if (qToDelete == MAX_QUESTIONS)  //If we are looking to delete the last question, we should delete it and make it NULL
			{
				delete questions[qToDelete-1];
				questions[qToDelete-1] = NULL;
				break;
			}
			if ((i + (qToDelete-1)) == MAX_QUESTIONS-1) //If we have reached the last question, but we are using it's memory elsewhere we should just make the pointer null
			{
				questions[i+ (qToDelete - 1)] = NULL;
				break;
			}
			if (!questions[i + qToDelete]) { //If the next index is Null, we have reached the end of our questions
				if (i == 0) //If our loop has just started, this means this is the question we must delete
				{
					delete questions[i + (qToDelete - 1)];
				}
				questions[i + (qToDelete - 1)] = NULL; //NULL it, it's memory is being used elsewhere
				break;
			}
			else { //If there is a another question
				if (i == 0) //First loop, this is the question we wish to delete
				{
					delete questions[i + (qToDelete - 1)];
				}
				questions[i + (qToDelete - 1)] = (questions[i + qToDelete]); //Copy by reference the next index into the previous index
			}

			i++;
		}
		cout << "Deleting question #" << qToDelete << "\n"; //Output the question they wished to delete
	}
	return true;
}



/********************************************************************************************************
* Function Name: operator[]
* Purpose: Returns the reference to Question of the index the user wishes to receive, if they ask for an invalid index or a question that doesnt exist
*		   Return a fake Question with invalid data.
* Function in parameters: Unsigned int index
* Function out parameters: Question
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
Question Exam::operator[](unsigned int index)
{
	if(this->questions[index-1] && index > 0 && index <=MAX_QUESTIONS) //If the current question exists and is within index range
	{
		Question q = Question(*this->questions[index-1]);
		return q;
	}
	Question question = Question("Invalid question",-1,NULL); //Else we have a invalid question, create question with invalid information
	return question;
}

/********************************************************************************************************
* Function Name: operator<<
* Purpose: Overloaded output operator, prints out the information about the exam, the title and the question calls the
*		   overloaded question operator<< to output the Question and it's answers
* Function in parameters: Ostream, Exam
* Function out parameters: Reference to ostream
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
ostream& operator<<(ostream& ostream,Exam& exam)
{
	ostream<<"Title "<<exam.title<<"\n";
	for (int i=0;i<exam.MAX_QUESTIONS;i++) //Iterate through the questions
	{
		if(exam.questions[i])  //If the question exists we should print it
		{
			ostream<<"Question "<<i+1<<": \n";
			Question question = Question(*exam.questions[i]); 
			ostream<<question; //Call overloaded output operator for question
			
		}
		else //Else we have no more question's to print
		{
			break;
		}
	}
	return ostream;
}