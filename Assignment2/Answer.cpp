/********************************************************************************************************
* FileName: Answer.cpp
* Version: 1.0
* Author: Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 - C++
* Lab section: 012
* Assignment Number: 2
* Assignment Name: Overloaded Operators
* Due Date: March 15, 2015
* Submission Date: March 15, 2015
* Professor's Name: Andrew Tyler
* Purpose: Answer cp includes Overloaded constructor operator, destructor to release dynamic memory,
*		   Copy constructor, overloaded assignment operator and overloaded << output
********************************************************************************************************/
#include "Answer.h"
/********************************************************************************************************
* Function Name: ~Answer
* Purpose: Deconstructor for the Answer structure, deletes the dynamic memory for the Answer text
* Function in parameters: None
* Function out parameters:None
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
 Answer::~Answer(){
	 delete aText;
 }
 /********************************************************************************************************
* Function Name: Answer
* Purpose: Overloaded Constructor for Answer, creates a Answer with the desired parameters
* Function in parameters: Char * text, bool b_correct
* Function out parameters: Question
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
 Answer::Answer(char* text, bool b_correct)
 {
	 this->aText = new char[strlen(text)+1]; //Allocate space for string
	 strcpy(this->aText,text);
	 this->b_correct = b_correct;
 }

 /********************************************************************************************************
* Function Name: Answer
* Purpose: Copy Constructor for Answer, creates a copy of the parameter Answer
* Function in parameters: Unsigned int index
* Function out parameters: Question
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
 Answer::Answer(Answer& answer)
 {
	 this->aText = new char[strlen(answer.aText)+1]; //Allocate space for string
	 strcpy(this->aText,answer.aText); //Strcpy text
	 this->b_correct = answer.b_correct;
 }

 
 /********************************************************************************************************
* Function Name: operator=
* Purpose: Overloaded assignment opeerator, returns a copy of the passed in answer
* Function in parameters: Answer& answer
* Function out parameters: *this(Answer)
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
 Answer& Answer::operator=(Answer& answer)
 {
	 if(answer.aText != 0) //If the pointer to answer text is not 0, we have a valid Answer 
	 {
		this->aText = new char[strlen(answer.aText)+1]; //Allocate space for text
		strcpy(this->aText,answer.aText); //strcpy the text
		this->b_correct = answer.b_correct;
	 }
	 return *this; //Return copy of the object
 }

 
 /********************************************************************************************************
* Function Name: operator<<
* Purpose: Overloaded output operator, used to print out the Answer's text
* Function in parameters: ostream& ostream, Answer& answer
* Function out parameters: ostream
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
 ostream& operator<<(ostream& ostream,Answer& answer)
 {
	 if(answer.aText != NULL) //If the answer text is valid, we have a valid answer
	 {
		ostream<<answer.aText;
	 }
	 return ostream;
 }