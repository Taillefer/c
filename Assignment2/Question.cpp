/********************************************************************************************************
* FileName: Question.cpp
* Version: 1.0
* Author: Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 - C++
* Lab section: 012
* Assignment Number: 2
* Assignment Name: Overloaded Operators
* Due Date: March 15, 2015
* Submission Date: March 15, 2015
* Professor's Name: Andrew Tyler
* Purpose: Question.cpp, Includes the Overloaded Constructor to create Question, destructor to release dynamic memory,
*		   Copy constructor, and overloaded << operator.
*			
********************************************************************************************************/
#include "Answer.h"
#include "Question.h"
#include <string.h>
#include <ostream>
/********************************************************************************************************
* Function Name: ~Question
* Purpose: Deconstructor for the Question structure, deletes the dynamic memory for the Question text
* Function in parameters: None
* Function out parameters:None
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
Question::~Question(){
	delete qText;
}

/********************************************************************************************************
* Function Name: Question
* Purpose: Overloaded constructor for the Question Class, creates a Question given the text, mark and pointer to Answer
* Function in parameters: char * text, unsigned int mark, Answer* answer
* Function out parameters: Question
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
Question::Question(char* text,unsigned int mark,Answer* answer)
{
	this->qText = new char[strlen(text)+1]; //Allocate space for the text
	strcpy(this->qText,text);
	this->mark = mark;
	if(this->mark != -1) //Check to see if the question is a valid question
	{
		for(int i=0;i<this->MAX_ANSWERS;i++) //Iterate through the answers
		{
			this->answers[i] = answer[i];  //Call overloaded assignment answer operator
		}
	}
}

/********************************************************************************************************
* Function Name: Question
* Purpose: Copy Constructor for the Question operator, Creates a copy of the Question
* Function in parameters: Question& question
* Function out parameters: Question
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
Question::Question(Question& question) 
{
	this->qText = new char[strlen(question.qText)+1]; //Allocate space for the text
	strcpy(this->qText,question.qText);
	this->mark = question.mark;
	for(int i=0;i<MAX_ANSWERS;i++) { //Iterate through the answers
		this->answers[i] = question.answers[i]; 
	}
}

/********************************************************************************************************
* Function Name: operator<<
* Purpose: Overloaded << operator, outputs the Question's text and calls the overloaded << Answer operator
* Function in parameters: Unsigned int index
* Function out parameters: Question
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
 ostream& operator<<(ostream& ostream,Question& question)
 {
	 if(question.mark !=-1) //Check to see if the question is a valid Question
	 {
		ostream<<question.qText<<"\n"; //Output Question text
		for(int i=0;i<question.MAX_ANSWERS;i++){ //Iterate through the question answers
			ostream<<"Ans: #"<<i+1<<" : "<<question.answers[i]<<"\n"; //Call overloaded << Answer Operator
		 }
	 }
	 else { //Else There is no Question available
		 ostream<<"Invalid question index, no question exists";
	 }
	 ostream<<"\n\n";
	 return ostream;
 }

		

