/***************************************************************************************************************************************************************************** 
Filename:				ass0.c
Version:				1.0
Author:					Brodie Taillefer
Student No:				040 757 711
Course Name/Number:		Numerical Computing		CST 8219
Lab Sect:				
Assignment #:			1
Assignment name:		Assignment #0
Due Date:				January 25, 2015
Submission Date:		January 23, 2015
Professor:				Andrew Tyler
Purpose:				Create an exam template in which the user is asked for the Exam Title, and any number of question upto 100.
						Each Question that the user input's is able to have anywhere from 1 to 5 answer's associated with it. The
						code provides 4 workable function, InitExam(),AddNewQuestion(),PrintQuestions(), and CleanUp().
******************************************************************************************************************************************************************************/

//#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
//#include <crtdbg.h>
#include <stdio.h>
#include <string.h>
#define MAX_QUESTIONS 100	// maximum questions per exam
#define MAX_ANSWERS 5		// maximum answers per question
typedef enum { FALSE = 0, TRUE }BOOL;

typedef struct
{
	char* aText;
	BOOL correct;
}Answer;

typedef struct
{
	char* qText;
	unsigned int mark;
	Answer answers[MAX_ANSWERS];
}Question;

typedef struct
{
	char* title;
	Question* questions[MAX_QUESTIONS];
}Exam;

// forward declarations
void InitExam(Exam*);
BOOL AddNewQuestion(Exam*);
BOOL DeleteQuestion(Exam*);
BOOL PrintQuestions(Exam*);
BOOL CleanUp();                                                   

BOOL b_running = TRUE;

/******************************************************************************************************************************************************************************
Function name:				main
Purpose:					main function
								Takes user input and call's the desired functions
In parameters:				none
Our parameters:				0 for succesfull execution
Version:					1.0
Author:						Brodie Taillefer
******************************************************************************************************************************************************************************/
int main(void)
{
	int i_response;
	Exam e;
	InitExam(&e);

	while (b_running)
	{

		printf("\nPlease enter your choice\n");
		printf("1. Add a New Question\n");
		printf("2. Delete a Question\n");
		printf("3. Print the Questions\n");
		printf("4. Quit\n");

		scanf("%d", &i_response);
		switch (i_response)
		{
		case 1:
			if (!AddNewQuestion(&e))
				return 1;
			break;
		case 2:
			if (!DeleteQuestion(&e))
				return 1;
			break;
		case 3:
			if (!PrintQuestions(&e))
				return 1;
			break;
		case 4:
			CleanUp(&e);
			b_running = FALSE;
			break;
		}

	}
	return 0;
}

/******************************************************************************************************************************************************************************
Function name:				InitExam
Purpose:					Intialize the exam Name of the exam and intialize the array of null pointers
In parameters:				Pointer to Exam
Our parameters:				void return
Version:					1.0
Author:						Brodie Taillefer
******************************************************************************************************************************************************************************/
void InitExam(Exam *pExam) {
	printf("Enter the name of the exam: ");
	char titleBuffer[100];
	fgets(titleBuffer, 100, stdin);
	fflush(stdin);
	char *examName = (char *)malloc(strlen(titleBuffer)+1); //Allocated memory for the exam title with just enough bytes for the string
	strcpy(examName, titleBuffer);
	pExam->title = examName;
	for (int i = 0; i < MAX_QUESTIONS; i++) { //Allocate all 100 array of pointers to NULL
		pExam->questions[i] = NULL;
	}
}

/******************************************************************************************************************************************************************************
Function name:				AddNewQuestion
Purpose:					Take user input and create a pointer to question, that includes a fixed array of answer's. The number of initilized answer's
								depends on the user's input.
In parameters:				Pointer to Exam
Our parameters:				BOOL, TRUE if succesfull. False if not.
Version:					1.0
Author:						Brodie Taillefer
******************************************************************************************************************************************************************************/
BOOL AddNewQuestion(Exam *pExam) {
	int i, counter, j, numberOfAnswers = 0;
	for (i = 0; i < MAX_QUESTIONS; i++) { // What is the next question that is NULL? This is will our next question
		if (pExam->questions[i] == 0) {
			break;
		}
	}
	Question *question = (Question *)malloc(sizeof(Question)); //Malloc pointer to question for our new question
	printf("Enter the new question text: ");
	char qTextBuffer[100]; 
	fflush(stdin);
	fgets(qTextBuffer, 100, stdin);
	question->qText = (char *)malloc(strlen(qTextBuffer)+1);
	strcpy(question->qText, qTextBuffer);
	fflush(stdin);
	printf("Please enter the new question mark: ");
	scanf_s("%d", &question->mark);
	printf("Please enter the number of answers, up to a max of 5: ");
	scanf("%d", &numberOfAnswers);
	while (numberOfAnswers <= 0 || numberOfAnswers > 5) {
		printf("Error: Please enter the number of answers , up to a max of 5: ");
		scanf_s("%d", &numberOfAnswers);
	}

	for (j = 0; j < MAX_ANSWERS; j++) {
		question->answers[j].correct = -1;
	}
	
	for (counter = 0; counter < numberOfAnswers; counter++) 
		fflush(stdin);
		printf("Please enter answer number: %d :\n", counter + 1);
		char aTextBuffer[100];
		fgets(aTextBuffer, 100, stdin);
		question->answers[counter].aText = (char *)malloc(strlen(aTextBuffer)+1);
		strcpy(question->answers[counter].aText, aTextBuffer);

		printf("Is it correct? (0 = no, 1 = yes)?: ");
		question->answers[counter].correct = malloc(sizeof(int));
		fflush(stdin);
		scanf_s("%d", question->answers[counter].correct);
		while (question->answers[counter].correct != 1 || question->answers[counter].correct != 0) {
			printf("Invalid input: Is it correct? (0 = no, 1 = yes): ");
			scanf_s("%d", question->answers[counter].correct);
		}
	}
	pExam->questions[i] = question;
	return TRUE;
}

/******************************************************************************************************************************************************************************
Function name:				PrintQuestions
Purpose:					Print out the information located in the array of pointers to question, aswell as the exam title.
In parameters:				Pointer to Exam
Our parameters:				BOOL, TRUE if succesfull. False if not.
Version:					1.0
Author:						Brodie Taillefer
******************************************************************************************************************************************************************************/
BOOL PrintQuestions(Exam *pExam) {
	int i, counter;
	printf("TITLE: %s\n", pExam->title);
	for (i = 0; i < MAX_QUESTIONS; i++) {
		if (!pExam->questions[i] <= 0) {
			printf("\nQUESTION %d. Mark = %d\n", i + 1, pExam->questions[i]->mark);
			printf("%s\n", pExam->questions[i]->qText);
			printf("ANSWERS\n");
			for (counter = 0; counter < MAX_ANSWERS; counter++) {
				if (pExam->questions[i]->answers[counter].correct != -1) {
					printf("Ans #%d\n", counter + 1);
					printf(" %s\n", pExam->questions[i]->answers[counter].aText);
				}
			}	
		}
		else {
			if (i == 0) {
				printf("There are no Questions in this exam.\n");
			}
			break;
		}

	}
	return TRUE;
}

/******************************************************************************************************************************************************************************
Function name:				DeleteQuestions
Purpose:					Delete a pointer in the array of pointers to questions, deleteing the question. This function is not implemented in version 1.0
In parameters:				Pointer to Exam
Our parameters:				BOOL, TRUE if succesfull. False if not.
Version:					1.0
Author:						Brodie Taillefer
******************************************************************************************************************************************************************************/
BOOL DeleteQuestion(Exam *pExam) {
	return TRUE;
}

/******************************************************************************************************************************************************************************
Function name:				CleanUP
Purpose:					Free all dynamic memory on the heap that was allocated during program execution. 
In parameters:				Pointer to Exam
Our parameters:				BOOL, TRUE if succesfull. False if not.
Version:					1.0
Author:						Brodie Taillefer
******************************************************************************************************************************************************************************/
BOOL CleanUp(Exam *pExam) {	
	int i, j;
	for (i = 0; i < MAX_QUESTIONS; i++) {
		if (!pExam->questions[i] <= 0) {
			free(pExam->questions[i]->qText);
			free(pExam->title);
			for (j = 0; j < MAX_ANSWERS; j++) {
				if (pExam->questions[i]->answers[j].correct != -1) {
					free(pExam->questions[i]->answers[j].aText);
				}
			}
			free(pExam->questions[i]);
		}
	}
	return TRUE;
	exit(0);
}




