/******************************************************************************************************
* Student Name: Brodie Taillefer
* Student Number: 040 757 711
* Assignment 1 - ExamTemplate
* Course Name: CST 8219 - C++
* Lab Section: 012
* Professor: Andrew Tyler
* Due Date: February 16
* Submission Date: TBD
* Source Files: ass1.cpp
*******************************************************************************************************/

/*******************************************************************************************************
* File Name: ass1.cpp
* Version: 1.0
* Author: Brodie Taillefer
* Purpose: The purpose of the program is to setup an exam template to allow the user to add questions and answers
*			to the Exam, and print out the questions/answers. The user can also delete any question,if valid.
*******************************************************************************************************/

#include "Answer.h"
#include "Question.h"
#include "Exam.h"
#include <iostream>
#include <crtdbg.h>
using namespace std;
#define _CRTDBG_MAP_ALLOC

/********************************************************************************************************
* Function Name: main
* Purpose: Main function for the exam schedule. Displays a menu and allows the to choose whether to add a question
*		   Print questions, delete questions, and quit. 
* Function in parameters: void
* Function out parameters: int
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/

int main(void)
{
	char i_response;
	bool b_running = true;
	Exam e;

	while(b_running)
	{
		cout<<"\nPlease enter your choice\n";
		cout<<"1. Add a New Question\n";
		cout<<"2. Delete a Question\n";
		cout<<"3. Print the Questions\n";
		cout<<"4. Quit\n";
		cin>>i_response;

		switch(i_response)
		{
		case '1':
			if(!e.AddNewQuestion())
				return 1;
			break;
		case '2':
			if(!e.DeleteQuestion())
				return 1;
			break;
		case '3':
			if(!e.PrintQuestions())
				return 1;
			break;
		case '4':
			b_running=false;
			break;
		}
	}
	return 0;

}

