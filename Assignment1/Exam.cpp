/********************************************************************************************************
* FileName: Exam.cpp
* Version: 1.0
* Author: Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 - C++
* Lab section: 012
* Assignment Number: 1
* Assignment Name: ExamTemplate in C++
* Due Date: February 15
* Submission Date: TBD
* Professor's Name: Andrew Tyler
* Purpose: Exam.cpp, Includes the functions for the Exam class. Includes Exam constructor, AddNewQuestion, DeleteQuestion,
*		   PrintQuestions, and the Exam class deconstructor.
********************************************************************************************************/

#include "Answer.h"
#include "Question.h"
#include "Exam.h"
#include <iostream>
using namespace std;

/********************************************************************************************************
* Function Name: Exam()
* Purpose: Constructor for the Exam Class. Intializes Exam objects. Asks user input for the Exam title, and intializes all questions to 0
* Function in parameters: None
* Function out parameters:None
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
Exam::Exam() {
	char tempTitle[255]; //Allocate max char size as a buffer 
	cout<<"What is the title of the exam?\n";
	cin.getline(tempTitle,100); //Get the user input upto a max of 100 chars
	title = new char[strlen(tempTitle)+1];
	strcpy(title,tempTitle);
	fflush(stdin);
	for(int i=0;i<MAX_QUESTIONS;i++) //Intialize all the questions with 0
	{
		questions[i] = 0;
	}
}

/********************************************************************************************************
* Function Name: AddNewQuestion
* Purpose: Add a new question to the Exam and it's Answers. Asks user input for Question text, and marks. Checking 
*		   to see if the what the user enter's is within valid range.
* Function in parameters: None
* Function out parameters:bool
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
bool Exam::AddNewQuestion() {
	int i,j,numAnswers = 0,correct;
	if(MAX_QUESTIONS <= 0) { // If the MAX_QUESTIONS is invalid
		cout<<"Error: MAX_QUESTIONS IS : "<<MAX_QUESTIONS<<".\n";
		return true;
	}
	for(i = 0;i < MAX_QUESTIONS; i++)  //Find the next question that can be used
	{
		if(!questions[i]) //If the question is 0, we found the next question to use
		{
			break;
		}
		else if(i == MAX_QUESTIONS-1) { //We have reached MAX_QUESTIONS we cannot add anymore
			printf("Reached Max Questions");
			return true;
		}
	}
	Question *question = new Question();
	fflush(stdin);
	cout<<"Please enter the new question text: ";
	char tempQText[255]; //Temp buffer for the question text
	cin.getline(tempQText,255); //Get user input
	question->qText = new char[strlen(tempQText)+1]; //Allocate space in the question text for the text
	strcpy(question->qText,tempQText); //strcpy into the dynamic memory location
	cout<<"\nPlease enter the new question mark: ";
	cin>>question->mark; //Take user input for question's mark
	if(MAX_ANSWERS <=0) {
		cout<<"Error: MAX_ANSWERS is : "<<MAX_ANSWERS<<".\n";
		return true;
	}
	cout<<"\nPlease enter the number of answers, up to a max of : "<<MAX_ANSWERS<<"\n";
	cin>>numAnswers; //Take user input for number of answers for the question
	while(numAnswers <=0 || numAnswers > MAX_ANSWERS) //Check if the user's input is valid
	{
		cout<<"Error: please enter the number of answers, up to a max of "<<MAX_ANSWERS<<": ";
		cin>>numAnswers;
	}
	for(j=0;j<numAnswers;j++) //Iterate through for the number of answer's they wish to enter
	{
		char tempAText[255]; //Temp location for Answer text
		cout<<"Please enter answer "<<j+1<<" \n";
		fflush(stdin);
		cin.getline(tempAText,255); 
		question->answers[j].aText = new char[strlen(tempAText)+1];
		strcpy(question->answers[j].aText,tempAText);
		cout<<"Is it correct? (0 = no, 1= yes)?: ";
		cin>>correct; 
		while(correct != 1 && correct != 0) //Check to see if user's input is valid
		{
			cout<<"Error: Please enter a valid correct answer (0 = no, 1 =yes)?: ";
			cin>>correct;
		}
		question->answers[j].correct = correct;
	}
	questions[i] = question; //Allocate the array of pointers to Question objects at index i to our question.
	return true;
}

/********************************************************************************************************
* Function Name: DeleteQuestion
* Purpose: Used to delete a question in the Exam object. Once the quesiton is deleted, the array of pointers
*		   to questions is packed to leave no empty spots.
* Function in parameters: None
* Function out parameters: bool
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
bool Exam::DeleteQuestion(){
	int qToDelete;
	int i = 0;
	cout<<"Please enter the number of question to delete: (1,2,3,4...)";
	cin>>qToDelete; 
	while(qToDelete <=0 || qToDelete >MAX_QUESTIONS-1) //If the question they want to delete is within the range
	{
		fflush(stdin);
		cout<<"Error: Please enter the number of question to delete: (1,2,3,4...)";
		cin>>qToDelete;
	}
	fflush(stdin);
	if(!questions[qToDelete-1]) { //If the Question pointer is 0, the user tried to delete an empty question
		cout<<"Empty Question";
	}
	else {
		while(questions[i+(qToDelete-1)])  //We need to pack the array of pointers to questions until it is fully packed
		{
			questions[i+(qToDelete-1)] = questions[i+(qToDelete)]; //The make the question they wish to delete equal to the question at the next index
			questions[i+(qToDelete)] = 0; //The next index will now be 0
			i++;
		}
		cout<<"Deleting question #"<<qToDelete<<"\n"; //Output the question they wished to delete
	}
	return true;
}

/********************************************************************************************************
* Function Name: PrintQuestions
* Purpose: Used to print the questions located in the exam.
* Function in parameters: None
* Function out parameters: bool
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
bool Exam::PrintQuestions(){
	int i=0;
	int j=0;
	cout<<"TITLE:"<<title<<"\n"; //output the Exam title
	while(questions[i]) //
	{
		if(i == MAX_QUESTIONS) { //If we reached MAX_QUESTIONS we are done printing
			return true;
		}
		cout<<"QUESTION "<<i+1<<"  "; 
		cout<<"Mark = "<<questions[i]->mark<<"\n";
		cout<<questions[i]->qText<<"\n";
		cout<<"ANSWERS\n";

		for(j=0;j<MAX_ANSWERS;j++) { //Iterate through upto the MAX_ANSWERS 
			if(questions[i]->answers[j].aText != 0) { //If the answer text is not null we can print the question because it exists
			cout<<"Ans"<<j+1<<"\n";
			cout<<questions[i]->answers[j].aText<<"\n";
			}
		}
		i++;
		cout<<"\n\n";
	}
	if(i == 0) //If we exit out at i==0, there are no questions
	{
		cout<<"There are no questions";
	}
	return true;
}
/********************************************************************************************************
* Function Name: ~Exam
* Purpose: Deconstructor for the Exam class, deletes the pointer to questions in the array, and the exam title.
* Function in parameters: None
* Function out parameters: None
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
Exam::~Exam() {
	int i=0;
	while(questions[i]) { //While the question is valid and exists
		delete questions[i]; //Delete the pointer to question at index i
		i++;
	}
	delete[] title; //Delete the exam title
   _CrtDumpMemoryLeaks();
}
