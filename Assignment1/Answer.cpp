/********************************************************************************************************
* FileName: Answer.cpp
* Version: 1.0
* Author: Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 - C++
* Lab section: 012
* Assignment Number: 1
* Assignment Name: ExamTemplate in C++
* Due Date: February 15
* Submission Date: TBD
* Professor's Name: Andrew Tyler
* Purpose: Answer cpp, includes the struct for Answer objects. It includes the deconstructor for Answer objects.
********************************************************************************************************/
#include "Answer.h"
/********************************************************************************************************
* Function Name: ~Answer
* Purpose: Deconstructor for the Answer structure, deletes the dynamic memory for the Answer text
* Function in parameters: None
* Function out parameters:None
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
 Answer::~Answer(){
	 delete aText;
 }