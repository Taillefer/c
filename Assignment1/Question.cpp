/********************************************************************************************************
* FileName: Question.cpp
* Version: 1.0
* Author: Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8219 - C++
* Lab section: 012
* Assignment Number: 1
* Assignment Name: ExamTemplate in C++
* Due Date: February 15
* Submission Date: TBD
* Professor's Name: Andrew Tyler
* Purpose: Question.cpp, Includes the Question structure. Also includes the Question object destructor.
********************************************************************************************************/
#include "Answer.h"
#include "Question.h"
/********************************************************************************************************
* Function Name: ~Question
* Purpose: Deconstructor for the Question structure, deletes the dynamic memory for the Question text
* Function in parameters: None
* Function out parameters:None
* Version: 1.0
* Author: Brodie Taillefer
********************************************************************************************************/
Question::~Question(){
	delete qText;
};
