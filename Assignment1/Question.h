const unsigned int MAX_ANSWERS = 3;
struct Question
{
	char* qText;
	unsigned int mark;
	Answer answers[MAX_ANSWERS];
	Question():qText(0){};
	~Question();
};