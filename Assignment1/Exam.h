const unsigned int MAX_QUESTIONS = 5;
class Exam
{
public:
	char* title;
	Question* questions[MAX_QUESTIONS];
	Exam();
	~Exam();
	bool AddNewQuestion();
	bool DeleteQuestion();
	bool PrintQuestions();
};


